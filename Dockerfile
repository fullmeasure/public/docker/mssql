FROM registry.gitlab.com/fullmeasure/public/docker/sidekiq:16.04

MAINTAINER FME <dev@fullmeasureed.com>

COPY *.sh /data/

RUN chmod +x /data/*.sh

# Install packages
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y freetds-dev freetds-bin tdsodbc unixodbc unixodbc-dev sqsh libc6-dev && \
    rm -rf /var/lib/apt/lists/*
